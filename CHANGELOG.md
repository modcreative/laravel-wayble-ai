# Wayble AI for Laravel
Domain knowledge artificial intelligence framework for Laravel
---
## [1.1.0] - 2023-10-11

### Added
- Added support for Filament v3

---
## [1.0.5] - 2023-06-27

### Added
- Added env flag to enable/disable reply validation: `validate_replies => AI_VALIDATE_REPLIES` 

---
## [1.0.4] - 2023-06-27

### Fixed
- Fixed issue where irrelevant media was being returned

---
## [1.0.3] - 2023-06-26

### Fixed
- Fixed fatal error with `text area` field type not found due to typo
- Fixed missing back button issue

---
## [1.0.2] - 2023-06-26

### Updated
- Updated client side chat to use trainables instead of domains

### Fixed
- Fixed issue where logic for `isOnTopic()` was not set correctly
- Fixed names for data in the seeder
- Fixed issue where the welcome message was not being set for chat
- Fixed chat errors from left over syntax
- Fixed chat link on welcome page
- Fixed chatbot's `getNameAttribute()`

---
## [1.0.1] - 2023-06-23

### Fixed
- Fixed issue where logic for `isOnTopic()` was not set correctly
- Fixed names for data in the seeder

---
## [1.0.0] - 2023-06-21

### Added
- Added all code from the previous package up to version `v0.6.3`

### Updated
- Updated codebase to introduce new, branded vendor/package and namespace

### Fixed
- Fixed several issues with classification and context awareness
