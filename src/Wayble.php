<?php

namespace WaybleAI;

/**
 * @mixin WaybleAI
 */
class Wayble extends \Illuminate\Support\Facades\Facade
{
    protected static function getFacadeAccessor(): string
    {
        return 'wayble';
    }
}
