<?php

namespace WaybleAI;

use WaybleAI\Traits\InteractsWithAIService;
use WaybleAI\Traits\InteractsWithVectorStore;
use WaybleAI\Traits\TrainsWithAIService;

class WaybleAI implements Contracts\AIServiceContract, Contracts\VectorStoreContract
{
    use InteractsWithAIService;
    use InteractsWithVectorStore;
    use TrainsWithAIService;
}
