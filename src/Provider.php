<?php

namespace WaybleAI;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Http;
use OpenAI;
use OpenAI\Client;
use WaybleAI\Exceptions\MissingApiKeyException;

class Provider extends \Illuminate\Support\ServiceProvider
{
    public function register(): void
    {
        $this->app->singleton(WaybleAI::class, static fn() => new WaybleAI());

        $this->app->singleton(Client::class, static function () {
            $driver = Config::get('wayble-ai.drivers.ai');

            $key = Config::get(sprintf('wayble-ai.%s.key', $driver));
            $org = Config::get(sprintf('wayble-ai.%s.org', $driver));

            if (!is_string($key) || ($org !== null && !is_string($org)))
            {
                throw MissingApiKeyException::create();
            }

            return OpenAI::client($key, $org);
        });

        $this->app->singleton('vector', static function () {
            return Http::withHeaders([
                'Api-Key' => Config::get('wayble-ai.pinecone.key'),
                'Accept' => 'application/json',
                'Content-Type' => 'application/json'
            ])->baseUrl(Config::get('wayble-ai.pinecone.url'));
        });

        $this->app->alias(WaybleAI::class, 'wayble');
        $this->app->alias(Client::class, 'ai');
    }

    public function provides(): array
    {
        return [
            Client::class,
            WaybleAI::class,
        ];
    }

    public function boot(): void
    {
        $this->registerRoutes();
        $this->registerCommands();
        $this->registerPublishing();
    }

    public function registerRoutes(): void
    {
        $this->loadRoutesFrom(sprintf('%s/../routes/web.php', __DIR__));
    }

    public function registerCommands(): void
    {
        if ($this->app->runningInConsole())
        {
            $this->commands([
                \WaybleAI\Commands\WaybleChat::class,
                \WaybleAI\Commands\WaybleTrain::class,
            ]);
        }
    }

    public function registerPublishing(): void
    {
        $this->publishes([
            __DIR__ . '/../config/wayble-ai.php' => config_path('wayble-ai.php'),
        ], 'wayble-ai-config');

        $this->publishes([
            __DIR__ . '/../database/migrations' => database_path('migrations'),
            __DIR__ . '/../database/seeders' => database_path('seeders')
        ], 'wayble-ai-database');

        $this->publishes([
            __DIR__ . '/../filament/Resources' => app_path('Filament/Resources'),
            __DIR__ . '/../resources/views/filament' => resource_path('views/filament'),
            __DIR__ . '/../resources/views/vendor' => resource_path('views/vendor'),
        ], 'wayble-ai-filament');

        $this->publishes([
            __DIR__ . '/../resources/js' => resource_path('js'),
        ], 'wayble-ai-js');

        $this->publishes([
            __DIR__ . '/../public' => public_path(),
        ], 'wayble-ai-public');
    }
}
