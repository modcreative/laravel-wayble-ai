<?php

namespace WaybleAI\Contracts;

/**
 * Interface AIServiceContract
 *
 * @author Selvin Ortiz <selvin@waybleai.com>
 * @author Brennen Phippen <brennen@waybleai.com>
 *
 * @package WaybleAI\Contracts
 */
interface AIServiceContract
{
    public function train(TrainableContract $trainable, string $text, string $namespace, array $metadata = []): bool;

    public function chat(array $messages = []): ChatReplyContract;

    public function classify(string $text, array $subjects = [], string $expectedStringInReply = null): ChatReplyContract;

    public function validate(string $text, string $topic): ChatReplyContract;

    public function vectorize(string $text): array;
}
