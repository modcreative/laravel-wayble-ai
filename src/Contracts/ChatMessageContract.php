<?php

namespace WaybleAI\Contracts;

use WaybleAI\Features\Chatting\Role;

/**
 * Represents a message to pass around during chat delegation
 *
 * @author Selvin Ortiz <selvin@waybleai.com>
 * @author Brennen Phippen <brennen@waybleai.com>
 *
 * @package WaybleAI\Contracts
 */
interface ChatMessageContract
{
    public static function fromChatReply(ChatReplyContract $reply): ChatMessageContract;

    public function role(): Role;

    public function content(): string;

    /**
     * @return array<string, string>
     */
    public function asArray(): array;

    /**
     * @return array<string, string>
     */
    public function asPersistable(): array;
}
