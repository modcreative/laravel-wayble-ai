<?php

namespace WaybleAI\Contracts;

use WaybleAI\Snippets\Snippet;

interface SnippetResolverContract
{
    public function resolve(string $snippet, array $attributes = []): Snippet;
}
