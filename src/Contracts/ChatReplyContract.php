<?php

namespace WaybleAI\Contracts;

/**
 * Represents a response from the AI service
 *
 * @author Selvin Ortiz <selvin@waybleai.com>
 * @author Brennen Phippen <brennen@waybleai.com>
 *
 * @package WaybleAI\Contracts
 */
interface ChatReplyContract
{
    public static function fromAIServiceResponse(array $response): self;

    public function role(): string;
    public function content(): string;

    public function totalTokensUsed(): int;
    public function promptTokensUsed(): int;
    public function completionTokensUsed(): int;

    public function isOnTopic(): bool;
    public function isContextUnknown(string $expected = null): bool;

    public function extractDelegateName(): string;
}
