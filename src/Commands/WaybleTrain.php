<?php

namespace WaybleAI\Commands;

use Illuminate\Console\Command;
use WaybleAI\Models\Trainable;
use WaybleAI\Wayble;

class WaybleTrain extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'wayble:train {file} {namespace=wayble-ai-test}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Index text in a file into sentences that will be used as knowledge for chat';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {
        $text   = file_get_contents($this->argument('file'));
        $stored =  Wayble::trainInBatches(Trainable::query()->firstOrFail(), $text, $this->argument('namespace'));

        if (!$stored)
        {
            $this->error('Training failed');

            return self::FAILURE;
        }

        $this->info('Training completed');

        return self::SUCCESS;
    }
}
