<?php

namespace WaybleAI\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Inertia\Inertia;
use WaybleAI\Contracts\ChatContract;
use WaybleAI\Features\Chatting\ChatMessage;
use WaybleAI\Features\Chatting\Role;
use WaybleAI\Models\Chat;
use WaybleAI\Models\Trainable;

class ChatController extends Controller
{
    public function show(Trainable $trainable): \Inertia\Response
    {
        if (empty($trainable->statements()->first()))
        {
            Log::warning('No statements found for trainable: ' . $trainable->id);
        }

        $welcomeMessage = $trainable->chatbot()->first()->welcome_message ?? null;

        return Inertia::render('DemoChat', compact('trainable', 'welcomeMessage'));
    }

    public function ask(Request $request): array
    {
        $request->validate(['prompt' => 'required|string|max:255']);

        $prompt = $request->input('prompt');
        $chatId = $request->input('chatId');
        $trainableId = $request->input('trainableId');

        $trainable = Trainable::query()
            ->where('id', $trainableId)
            ->with('chatbot.agents.dialogues')
            ->firstOrFail();

        /** @var ChatContract $chat */
        $chat = Chat::query()
            ->with(['agent', 'dialogue'])
            ->firstOrCreate([
                'id' => $chatId,
                'trainable_id' => $trainableId
            ]);

        $chatbot = $trainable->chatbot;
        $agent = $chat->agent;
        $dialogue = $chat->dialogue;

        if ($agent && $dialogue)
        {
            $agent->setSelectedDelegate($dialogue);
            $chatbot->setSelectedDelegate($agent);
        }

        try
        {
            $message = $chat->reply($chatbot, new ChatMessage(Role::User, $prompt));
        }
        catch (\Throwable $e)
        {
            $message = new ChatMessage(Role::Assistant, $e->getMessage());
        }

        return [
            'reply' => $message->content(),
            'media' => $message->relevantMedia(),
            'agentId' => $chat->agent->id ?? null,
            'chatId' => $chat->id ?? null,
            'trainableId' => $trainable->id ?? null
        ];
    }

    public function history(Request $request): array
    {
        $chat = Chat::query()
            ->where('id', $request->input('chatId'))
            ->first();

        if (empty($chat->messages))
        {
            return [
                'history' => []
            ];
        }

        return [
            'history' => $chat->messages
        ];
    }
}
