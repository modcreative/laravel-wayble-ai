<?php

namespace App\Filament\Resources\TrainableResource\Widgets;

use App\Filament\Resources\TrainableResource;
use Filament\Widgets\Widget;
use WaybleAI\Models\Trainable;

class Training extends Widget
{
    public Trainable $record;

    protected static string $view = 'filament.resources.trainable-resource.widgets.training';

    protected function getViewData(): array
    {
        return [
            'trainingUrl' => TrainableResource::getUrl('training', [$this->record])
        ];
    }
}
