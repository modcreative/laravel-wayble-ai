<?php

namespace App\Filament\Resources\TrainableResource\Pages;

use App\Filament\Resources\TrainableResource;
use Filament\Actions\Action;
use Filament\Forms\Form;
use Filament\Resources\Pages\ViewRecord;
use Illuminate\Contracts\Support\Htmlable;

class ViewTrainable extends ViewRecord
{
    protected static string $resource = TrainableResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Action::make('Publish')
                ->icon('heroicon-o-bolt')
                ->color('gray')
                ->action(fn () => $this->record->publish())
                ->hidden(function () {
                    return $this->record->published_at;
                }),
            Action::make('Unpublish')
                ->icon('heroicon-o-eye-slash')
                ->color('gray')
                ->action(fn () => $this->record->unpublish())
                ->hidden(function () {
                    return !$this->record->published_at;
                }),
            Action::make('Edit')
                ->icon('heroicon-o-pencil')
                ->url(TrainableResource::getUrl('edit', [$this->record])),
        ];
    }

    public function getHeading(): string|Htmlable
    {
        return $this->record->name;
    }

    public function form(Form $form): Form
    {
        return $form;
    }

    protected function getHeaderWidgets(): array
    {
        return [
            TrainableResource\Widgets\Training::class,
            TrainableResource\Widgets\ChatLink::class
        ];
    }
}
