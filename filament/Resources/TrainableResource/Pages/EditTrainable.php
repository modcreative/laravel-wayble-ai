<?php

namespace App\Filament\Resources\TrainableResource\Pages;

use App\Filament\Resources\TrainableResource;
use Filament\Actions;
use Filament\Resources\Pages\EditRecord;

class EditTrainable extends EditRecord
{
    protected static string $resource = TrainableResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\DeleteAction::make(),
        ];
    }

    public function getRelationManagers(): array
    {
        return [];
    }

    protected function getRedirectUrl(): string
    {
        return TrainableResource::getUrl('view', [$this->record]);
    }
}
