<?php

namespace App\Filament\Resources;

use App\Filament\Resources\ChatResource\Pages;
use Filament\Forms\Form;
use Filament\Resources\Resource;
use Filament\Tables\Table;
use Filament\Tables;
use WaybleAI\Models\Chat;

class ChatResource extends Resource
{
    protected static ?string $model = Chat::class;

    protected static ?string $navigationIcon = 'heroicon-o-chat-bubble-left-right';

    public static function form(Form $form): Form
    {
        return $form;
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('id'),
                Tables\Columns\TextColumn::make('messages_count')
                    ->counts('messages'),
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\DeleteBulkAction::make(),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            ChatResource\RelationManagers\MessagesRelationManager::class
        ];
    }

    public static function getPages(): array
    {
        return [
            'index'  => Pages\ListChats::route('/'),
            'create' => Pages\CreateChat::route('/create'),
            'view'   => Pages\ViewChat::route('/{record}'),
            'edit'   => Pages\EditChat::route('/{record}/edit'),
        ];
    }
}
