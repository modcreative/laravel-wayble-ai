<?php

namespace App\Filament\Resources;

use App\Filament\Resources\TrainableResource\Pages;
use App\Filament\Resources\TrainableResource\RelationManagers;
use Filament\Forms;
use Filament\Forms\Form;
use Filament\Resources\Resource;
use Filament\Tables\Table;
use Filament\Tables;
use WaybleAI\Models\Trainable;

class TrainableResource extends Resource
{
    protected static ?string $model = Trainable::class;

    protected static ?string $navigationIcon = 'heroicon-o-building-office';

    protected static ?int $navigationSort = 1;

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\Section::make()
                    ->schema([
                        Forms\Components\TextInput::make('name')
                            ->required(),
                        Forms\Components\TextInput::make('handle')->prefix('@')
                            ->required(),
                    ])
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('name'),
                Tables\Columns\BadgeColumn::make('published_at')
                    ->color(fn($state) => $state ? 'success' : 'secondary')
                    ->formatStateUsing(fn($state) => $state ? 'Published' : 'Draft')
                    ->label('Published'),
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\ViewAction::make(),
            ])
            ->bulkActions([
                //Tables\Actions\DeleteBulkAction::make(),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            RelationManagers\StatementsRelationManager::class
        ];
    }

    public static function getPages(): array
    {
        return [
            'index'    => Pages\ListTrainables::route('/'),
            'create'   => Pages\CreateTrainable::route('/create'),
            'view'     => Pages\ViewTrainable::route('/{record}'),
            'edit'     => Pages\EditTrainable::route('/{record}/edit'),
            'training' => Pages\Training::route('/{record}/training'),
        ];
    }

    public static function getWidgets(): array
    {
        return [];
    }
}
