# Wayble AI by Mod Creative
Domain knowledge artificial intelligence framework for Laravel

## Usage

### Install
> If you're using the provided seeder, you must update your own seeder to run it, e.g. `$this->call(WaybleAISeeder::class)`.
```bash
# install package
composer require modcreative/laravel-wayble-ai

# export migrations and seeders
php artisan vendor:publish --tag="wayble-ai-database"
```

### Configure
```bash
# export config
php artisan vendor:publish --tag="wayble-ai-config"

# edit config
config/wayble-ai.php
```

### Filament
> If you're using filament in your Laravel app, you can export the resources provided by Wayble AI.
>
```bash
# export filament resources
php artisan vendor:publish --tag="wayble-ai-filament"
```

### Publish Public Resources

```bash
php artisan vendor:publish --tag="wayble-ai-public"
```

### Publish JS Resources

```bash
php artisan vendor:publish --tag="wayble-ai-js"
```

## Gotchas 💣
> We use `uuids` everywhere!
