<?php

namespace WaybleAI\Tests\TestClasses;

use WaybleAI\Traits\InteractsWithVectorStore;
use WaybleAI\Traits\TrainsWithAIService;

class DummyForTraining
{
    use TrainsWithAIService;
    use InteractsWithVectorStore;

    public function __construct(array $config = [])
    {
        foreach ($config as $key => $value)
        {
            $this->{$key} = $value;
        }
    }
}
