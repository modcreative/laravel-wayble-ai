<?php

namespace WaybleAI\Tests\TestClasses;

use WaybleAI\Traits\InteractsWithAIService;
use WaybleAI\Traits\InteractsWithVectorStore;

class DummyForChatting
{
    use InteractsWithAIService;
    use InteractsWithVectorStore;

    public function __construct(array $config = [])
    {
        foreach ($config as $key => $value)
        {
            $this->{$key} = $value;
        }
    }
}
