import { ref } from 'vue'
import { defineStore } from 'pinia'

export const useChatStore = defineStore(
    'trainables',
    () => {
        const trainables = ref('[]')

        function hasTrainableId(trainables, trainableId) {
            let matches = trainables.filter(d => {
                const trainable = JSON.parse(d)

                return trainable.trainableId == trainableId
            })

            return matches.length === 0 ? false : true
        }

        function dropTrainableId(trainables, trainableId) {
            let newTrainables = trainables.filter(d => {
                const trainable = JSON.parse(d)

                return trainable.trainableId != trainableId
            })

            return newTrainables
        }

        function addChatId(trainableId, chatId) {
            let trainables = JSON.parse(this.trainables)
            
            if (this.hasTrainableId(trainables, trainableId)) {
                trainables = this.dropTrainableId(trainables, trainableId)
            }

            let chat = JSON.stringify({
                'trainableId': trainableId,
                'chatId': chatId
            })

            trainables.push(chat)

            this.trainables = JSON.stringify(trainables)
        }

        function getChatId(trainableId) {
            const trainables = JSON.parse(this.trainables ?? '[]')

            let chatId  = ''

            trainables.forEach(d => {
                const trainable = JSON.parse(d)

                trainable.trainableId == trainableId
                    ? chatId = trainable.chatId
                    : ''

                return
            })

            return chatId
        }

        function clear(trainableId) {
            let trainables = JSON.parse(this.trainables ?? '[]')

            trainables = this.dropTrainableId(trainables, trainableId)

            this.trainables = JSON.stringify(trainables)
        }

        return {
            trainables,
            hasTrainableId,
            dropTrainableId,
            addChatId,
            getChatId,
            clear
        }
    },
    {
        persist: true,
    }
)
