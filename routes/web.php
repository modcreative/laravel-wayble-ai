<?php

use Illuminate\Routing\Middleware\SubstituteBindings;
use Illuminate\Support\Facades\Route;
use WaybleAI\Http\Controllers\ChatController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Demo Chat
Route::middleware([SubstituteBindings::class])
    ->prefix(config('wayble-ai.path'))
    ->group(function () {
        Route::get('/{trainable:handle}', [ChatController::class, 'show'])->name('demo.chat');
        Route::post('/{trainable:handle}', [ChatController::class, 'ask'])->name('demo.chat.ask');
        Route::post('/{trainable:handle}/history', [ChatController::class, 'history'])->name('demo.chat.history');
    }
);
